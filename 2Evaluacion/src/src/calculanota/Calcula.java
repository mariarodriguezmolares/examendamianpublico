/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package calculanota;

/**
 *
 * @author mrodriguezmolares
 */
public class Calcula {

    public static float notaMedia;

    public void Teorica(float teorica1, float teorica2) {

        teorica1 = (teorica1 * 20) / 100;
        teorica2 = (teorica2 * 80) / 100;
        float resultadoTeorico = teorica1 + teorica2;
    }

    public float Practico(float practico) {
        practico = (practico * 40) / 100;
        return practico;
    }

    public int Boletines(int boletines) {
        if (boletines > 37) {
            boletines = 2;
        } else if (boletines < 37) {
            if (boletines >= 29) {
                boletines = 1;
            } else if (boletines < 29) {
                boletines = 0;
            }
        }
        return boletines;
    }
        
    public float NotaMedia(float resultadoTeorico, float practico, float boletines) {

        resultadoTeorico = (resultadoTeorico * 40) / 100;
        practico = (practico * 40) / 100;

        notaMedia = resultadoTeorico + practico + boletines;

        System.out.println("Nota Teorica: " + resultadoTeorico);
        System.out.println("Nota Practica: " + practico);
        System.out.println("Nota Boletines: " + boletines);
        return notaMedia;
    }
}
