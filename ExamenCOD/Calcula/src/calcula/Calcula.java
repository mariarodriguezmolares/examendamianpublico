/*Ejercicio para calcular la nota media con teorico,práctico y boletines.
 */
package calcula;

import java.util.Scanner;

/**
 *
 * @author mrodriguezmolares
 */
public class Calcula {
    /* Ya hemos echo el primer ejercicio.
    * Ahora tenemos que realizar una segunda rama llamada V2.0
    * Se realiza un cambio para hacer un commit.
    */
    
    private static float teorica1;
    private static float teorica2;
    private static float practico;
    private static int boletines;


    public static void main(String[] args) {
        int bole;
        CalculaNota calcula = new CalculaNota();
        Scanner calcular = new Scanner (System.in);
        
        // Pedimos las notas y el número de boletines que tiene.
        System.out.println("Dime tu nota del primer examen teorico: ");
        teorica1 = calcular.nextFloat();
        System.out.println("Dime tu nota del segundo examen teorico: ");
        teorica2 = calcular.nextFloat();
        System.out.println("Dime tu nota del examen practico: ");
        practico = calcular.nextFloat();
        System.out.println("Dime el numero total de los boletines: ");
        bole = calcular.nextInt();
        boletines =  calcula.Boletines(bole);
        
        System.out.println("La nota media es igual a: "+calcula.NotaMedia(teorica1, practico, boletines));
        
    }
}
